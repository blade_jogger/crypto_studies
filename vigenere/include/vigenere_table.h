#include <ctype.h> // isalum
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int processing(char *text, char *key);
int encipher(char *plain, char *key, char *ciph);
int decipher(char *ciph, char *key, char *plain);
