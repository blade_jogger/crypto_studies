#!/bin/bash

if [ -d build ]
then
    rm -rf ./build
fi

mkdir -p build
cd build
cmake ..
make -j
cd ..

if [ -f build/vigenere ]
then
    echo "fresh!"
else
    echo "not built"
fi
