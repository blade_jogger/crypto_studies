#include "vigenere_table.h"

int main (int argc, char *argv[])
{
    char *plaintext, *keyword, *cipherfile, *secret_msg; 
    int ret = 0;

    if (argc != 5) {
        fprintf(stderr, "%s <plaintext> <keyword> <ciphertext> <deciphertext>\n", argv[0]);
        ret = 1;
        return ret;
    }

    size_t pt_len = strlen(argv[1]);
    size_t key_len = strlen(argv[2]);
    size_t ct_len = strlen(argv[3]);
    size_t sm_len = strlen(argv[4]);
    plaintext = calloc(1, pt_len + 1);
    memcpy(plaintext, argv[1], pt_len);
    keyword = calloc(1, key_len + 1);
    memcpy(keyword, argv[2], key_len);

    printf("** processing command line arguments\n");
    ret = processing(plaintext, keyword);
    if (0 != ret) {
        return ret;
    }
    printf("...processed\n");

    cipherfile = calloc(1, ct_len + 1);
    memcpy(cipherfile, argv[3], ct_len);

    printf("** enciphering message in %s\n", plaintext);
    ret = encipher(plaintext, keyword, cipherfile);
    printf("...enciphered\n");

    secret_msg = calloc(1, sm_len + 1);
    memcpy(secret_msg, argv[4], sm_len);
    printf("** deciphering %s\n", cipherfile);
    ret = decipher(cipherfile, keyword, secret_msg);
    printf("...deciphered\n");

    free(cipherfile);
    free(keyword);
    free(plaintext);
    return ret;
}
