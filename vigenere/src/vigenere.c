// TEST:
// the x var: can i remove the word ALPHABET and it work the same way?
//
//

#include "vigenere_table.h"

char ALPHABET[26] =
{
  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
  'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
};


int processing (char *plaintext, char *keyword)
{

    int ret = 0;

    FILE *pt = fopen(plaintext, "r");
    if (NULL == pt) {
        fprintf(stderr, "%s: does not exist\n", plaintext);
        ret = 2;
        goto cleanup0;
    }

    int endoffile = fseek(pt, 0, SEEK_END);
    if (0 != endoffile) {
        fprintf(stderr, "%s: error finding eof\n", plaintext);
        ret = 3;
        goto cleanup;
    }

    size_t keyword_len = strlen(keyword);

    long file_len = ftell(pt);
    if (file_len <= keyword_len) {
        fprintf(stderr, "%s: too long for given plaintext\n", keyword);
        ret = 4;
        goto cleanup;
    }

    int alpha_num_check = 0;
    for (int i = 0; i < keyword_len; i++) {
        alpha_num_check = isalnum(keyword[i]);
        if (0 == alpha_num_check) {
            fprintf(stderr, "%s: noncorformant characters\n", keyword);
            ret = 5;
        }
    }

cleanup:
    fclose(pt);
cleanup0:

    return ret;
}


int encipher (char *plaintext, char *key, char *ciphertext)
{
    int ret = 0 ;

    FILE *pt = fopen(plaintext, "r");
    if (NULL == pt) {
        fprintf(stderr, "%s: could not open\n", plaintext);
        ret = 6;
        goto end;
    }

    FILE *ct = fopen(ciphertext, "w");
    if (NULL == ct) {
        fprintf(stderr, "%s: unable to open\n", ciphertext);
        ret = 7;
        goto cleanup;
    }

    int plain_letter;
    int index_tracker = 0;
    size_t keyword_len = strlen(key);
    plain_letter = fgetc(pt);
    while (EOF != plain_letter) {
        char x = ALPHABET[key[index_tracker % keyword_len] - 97];
        int y = (x - 65) + (plain_letter - 97);
        if (25 < y) {
            y -= 26;
        }
        char cipher_letter = ALPHABET[y];
        fputc(cipher_letter, ct);
        index_tracker++;
        plain_letter = fgetc(pt);
    }

    fclose(ct);
cleanup:
    fclose(pt);
end:
    return ret;
}


int decipher (char *ciphertext, char *key, char *plaintext)
{
    int ret = 0;

    FILE *ct = fopen(ciphertext, "r");
    if (NULL == ct) {
        fprintf(stderr, "%s: could not open\n", ciphertext);
        ret = 8;
        goto end;
    }

    FILE *pt = fopen(plaintext, "w");
    if (NULL == pt) {
        fprintf(stderr, "%s: unable to open\n", plaintext);
        ret = 9;
        goto cleanup;
    }

    int cipher_letter;
    int index_tracker = 0;
    size_t keyword_len = strlen(key);
    cipher_letter = fgetc(ct);
    while (EOF != cipher_letter) {
        char x = ALPHABET[key[index_tracker % keyword_len] - 97];
        int y = (cipher_letter - 65) - (x - 65);
        if (0 > y) {
            y += 26;
        }
        char plain_letter = ALPHABET[y];
        fputc(plain_letter, pt);
        index_tracker++;
        cipher_letter = fgetc(ct);
    }

    fclose(pt);
cleanup:
    fclose(ct);
end:
    return ret;
}
